package com.phongsai.arissara.wdpviewdemo;

import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    int num1, num2;
    int sum;

    Button btn;
    EditText editText1;
    EditText editText2;
    TextView result;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn = (Button) findViewById(R.id.calButton);
        btn.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        editText1 = (EditText) findViewById(R.id.edit_text1);
        editText2 = (EditText) findViewById(R.id.edit_text2);
        result = (TextView) findViewById(R.id.show_text);

        num1 = Integer.parseInt(editText1.getText().toString());
        num2 = Integer.parseInt(editText2.getText().toString());

        sum = num1 + num2;

        result.setText(" "+sum);
    }
}

