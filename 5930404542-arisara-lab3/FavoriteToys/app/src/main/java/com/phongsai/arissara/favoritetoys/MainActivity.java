package com.phongsai.arissara.favoritetoys;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    public TextView greenText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main);

        greenText = (TextView) findViewById(R.id.green);
        greenText.setText("Green");
        greenText.setTextSize(30);
        greenText.setGravity(Gravity.CENTER);

    }
}
