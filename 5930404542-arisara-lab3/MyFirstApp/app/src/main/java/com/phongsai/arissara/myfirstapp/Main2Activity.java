package com.phongsai.arissara.myfirstapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {

    TextView textShow;
    String show_name, show_phone, show_st;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        textShow = findViewById(R.id.textShow);
        show_st = getString(R.string.sentence);

        Intent intent = getIntent();
        show_name = intent.getStringExtra("show_name");
        show_phone = intent.getStringExtra("show_phone");
        textShow.setText(show_name + " " + show_st + " " + show_phone);
    }
}
