package com.phongsai.arissara.myfirstapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button buttSubmit;
    TextView labelName, labelPhone;
    EditText nameBox, phoneBox;
    String buttThai,nameThai,phoneThai ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buttThai = getString(R.string.button_submit);
        nameThai = getString(R.string.label_name);
        phoneThai = getString(R.string.label_phone);

        buttSubmit = findViewById(R.id.buttSub);
        labelName = findViewById(R.id.labelName);
        labelPhone = findViewById(R.id.labelPhone);
        nameBox = findViewById(R.id.nameBox);
        phoneBox = findViewById(R.id.phoneBox);

        buttSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Main2Activity.class);

                intent.putExtra("show_name", nameBox.getText().toString());
                intent.putExtra("show_phone", phoneBox.getText().toString());

                startActivity(intent);
            }
        });
    }
}
