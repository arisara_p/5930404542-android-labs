package com.sudjunham.boonyapon;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.content.Intent;
import android.speech.RecognizerIntent;
import android.support.v7.widget.Toolbar;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;



public class MainActivity extends AppCompatActivity implements SearchToolbar.OnSearchToolbarQueryTextListner {

    SearchToolbar searchToolbar;
    List<Event_list> event_List_Arr = new ArrayList<Event_list>();
    ScrollView scrollView;
    ListView listView;
    MyListAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setSupportActionBar((Toolbar)findViewById(R.id.toolbar));
       getSupportActionBar().setTitle("");

        searchToolbar = new SearchToolbar(this,this,findViewById(R.id.search_layout));
        scrollView = findViewById(R.id.scrollView_main);

        scrollView.smoothScrollTo(0,0);

        listView = findViewById(R.id.list_view1);
        callEventAPI();
        adapter = new MyListAdapter(this,event_List_Arr);
        listView.setAdapter(adapter);


    }

    private void callEventAPI(){
        String url = "https://www.kku.ac.th/ikku/api/activities/services/topActivity.php";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("activities");


                            /*JSONArray sortedJsonArray = new JSONArray();

                            List<JSONObject> jsonValues = new ArrayList<JSONObject>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                jsonValues.add(jsonArray.getJSONObject(i));
                            }
                            Collections.sort( jsonValues, new Comparator<JSONObject>() {
                                //You can change "Name" with "ID" if you want to sort by ID
                                private static final String KEY_NAME = "dateSt";

                                @Override
                                public int compare(JSONObject a, JSONObject b) {
                                    String valA = new String();
                                    String valB = new String();

                                    try {
                                        valA = (String) a.get(KEY_NAME);
                                        valB = (String) b.get(KEY_NAME);
                                    }
                                    catch (JSONException e) {
                                        //do something
                                    }

                                    return valA.compareTo(valB);
                                    //if you want to change the sort order, simply use the following:
                                    //return -valA.compareTo(valB);
                                }
                            });

                            for (int i = 0; i < jsonArray.length(); i++) {
                                sortedJsonArray.put(jsonValues.get(i));
                            }*/


                            for(int i = 0 ; i < jsonArray.length();i++){
                                JSONObject activity_event = jsonArray.getJSONObject(i);
                                Event_list event_list = new Event_list();

                                String pDate = activity_event.getString("dateSt");
                                String pTimeST = activity_event.getString("timeSt");
                                String pTimeED = activity_event.getString("timeEd");


                                event_list.setName(activity_event.getString("title"));
                                event_list.setDate(dateThai(pDate) + pTime(pTimeST,pTimeED) );
                                event_list.setLocation(activity_event.getString("place"));
                                event_List_Arr.add(event_list);
                                adapter.notifyDataSetChanged();

                            }


                        } catch (JSONException e) {

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        AppController.getInstance().addToRequestQueue(request);
    }

    public static String dateThai(String strDate)
    {
        String Months[] = {
                "มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน",
                "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม",
                "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"};

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

        int year=0,month=0,day=0;
        try {
            Date date = df.parse(strDate);
            Calendar c = Calendar.getInstance();
            c.setTime(date);

            year = c.get(Calendar.YEAR);
            month = c.get(Calendar.MONTH);
            day = c.get(Calendar.DATE);

        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return String.format("%s %s %s", day,Months[month],year);
    }

    public static String pTime(String strtime , String timeED) throws ParseException {

            DateFormat parser = new SimpleDateFormat("a. HH.mm");
            Date dateST = parser.parse(strtime);
            Date dateEd = parser.parse(timeED);
            SimpleDateFormat formatter = new SimpleDateFormat("HH.mm");
            String formattedDateST = formatter.format(dateST);
            String formattedDateED = formatter.format(dateEd);
            return String.format(" เวลา %s - %s น.", formattedDateST,formattedDateED );

    }

    private void Toast(String s){
        Toast.makeText(this,s,Toast.LENGTH_SHORT).show();}



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_item,menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.ic_search:
                searchToolbar.openSearchToolbar();
                break;
        }
        return true;
    }

    /***** Get data from voice, because voice app will send data through intent & we want to receive */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==SearchToolbar.VOICE_INTENT_REQUEST_CODE && data != null)
        {
            ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            Toast.makeText(this, "User Query: "+result.get(0), Toast.LENGTH_SHORT).show();
        }

    }


    /******* The following method will invoke when user Change or Submit text in SearchToolbar*/
    @Override
    public void onQueryTextSubmit(String query) {
        Toast.makeText(this, "User Query: "+query , Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onQueryTextChange(String editable) {
    }

}
