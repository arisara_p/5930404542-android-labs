package com.sudjunham.boonyapon;

import android.app.Activity;
import android.content.Context;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import android.widget.BaseAdapter;
import android.widget.TextView;

import com.android.volley.Response;

import org.json.JSONObject;

import java.util.List;

/**
 * Created by Belal on 9/14/2017.
 */

//we need to extend the ArrayAdapter class as we are building an adapter
public class MyListAdapter extends BaseAdapter {

    //the list values in the List of type hero
    List<Event_list> eventList;
    LayoutInflater inflater;
    Activity activity;

    public MyListAdapter(Activity activity, List<Event_list> eventList) {
        this.activity = activity;
        this.eventList = eventList;
    }

    @Override
    public int getCount() {
        return eventList.size();
    }

    @Override
    public Object getItem(int position) {
        return eventList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.item_listview_layout, null);

        //getting the view elements of the list from the view
        TextView textViewName = convertView.findViewById(R.id.event_name);
        TextView textViewDate = convertView.findViewById(R.id.event_time);
        TextView textViewLocation = convertView.findViewById(R.id.event_location);

        Event_list eventtt = eventList.get(position);


        textViewName.setText(eventtt.getName());
        textViewDate.setText(eventtt.getDate());
        textViewLocation.setText(eventtt.getLocation());

        return convertView;
    }

}