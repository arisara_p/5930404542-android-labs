package com.phongsai.arissara.mycalculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import java.util.Calendar;
import java.util.Date;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {
    EditText numIp1, numIp2;
    TextView showCal;
    Button calButt;
    RadioGroup rbGroup;
    float resultt = 0, num1, num2;
    Switch swOF;
    Boolean checkZero = true;
    Calendar rightNow = Calendar.getInstance();
    long timeResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        numIp1 = (EditText) findViewById(R.id.num1);
        numIp2 = (EditText) findViewById(R.id.num2);
        showCal = (TextView) findViewById(R.id.showNum);
        calButt = (Button) findViewById(R.id.button);
        rbGroup = (RadioGroup) findViewById(R.id.rbGroup);
        swOF = (Switch) findViewById(R.id.swOF);
        swOF.setOnCheckedChangeListener(this);

        rbGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                calculate(checkedId);
            }
        });

        calButt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v == calButt) {
                    calculate(rbGroup.getCheckedRadioButtonId());
                }
            }
        });
    }

    private void acceptNumbers() {
        try {
            num1 = Float.parseFloat(numIp1.getText().toString());
            num2 = Float.parseFloat(numIp2.getText().toString());

        } catch (Exception e) {
            showToast("Please enter only number");
        }
    }

    private void calculate(int id) {
        long timeBefore = rightNow.get(Calendar.MILLISECOND);
        acceptNumbers();
            switch (id) {
                case R.id.addd:
                    resultt = num1 + num2;
                    break;
                case R.id.sub:
                    resultt = num1 - num2;
                    break;
                case R.id.mult:
                    resultt = num1 * num2;
                    break;
                case R.id.div:
                    if (num2 != 0) {
                        resultt = num1 / num2;
                    } else {
                        showToast("Please divide by a non zero number");
                    }
                    break;
            }
                showCal.setText(" = " + resultt);
                long timeAfter = rightNow.get(Calendar.MILLISECOND);
                timeResult = timeBefore - timeAfter;
                Log.i("Calculation", "computation time = " + timeResult);
    }

    private void showToast (String msg){
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            swOF.setText("ON ");
        }
        else swOF.setText("OFF ");
    }
}
